#!/usr/bin/env bash
IFS=$'\n\t'
set -euo pipefail

cd "$(dirname "$(readlink -f "${0:-.}")")"

source ./common.sh
source ./config.sh

version="$(get_image_version_from_image_name "${IMAGE_NAME}" 2>/dev/null || true)"

if [[ -z "${version}" ]]
then
	# there's no existing image, build it from the seed
	echo "Creating docker image from seed..."
	SEED_NAME="$(find . -maxdepth 1 -name "${SEED_PREFIX}*${SEED_SUFFIX}" | sort | tail -n 1)"
	IMAGE_VERSION="$(basename "${SEED_NAME}" "${SEED_SUFFIX}" | grep -o '[0-9]\+$')"
	echo "Using seed : '${SEED_NAME}'"
	# Verify seed
	echo "Verifying seed..."
	sha256sum -c "${SEED_NAME}".sha256
	gpg --verify "${SEED_NAME}".asc "${SEED_NAME}"
	echo "Building docker image..."
	docker_build --squash --build-arg=SEED_NAME="${SEED_NAME}"
	echo "Tagging image as latest..."
	docker tag "${IMAGE_NAME}:${IMAGE_VERSION}" "${IMAGE_NAME}:latest"
	echo "Image available as '${IMAGE_NAME}:${IMAGE_VERSION}'"
else
	# Use the existing latest image to update
	echo "Updating existing image '${IMAGE_NAME}:latest'"
	docker_run_build "${IMAGE_NAME}:latest" bash < ./docker-update-internal.sh
	container_id="$(get_container_id_from_ancestor "${IMAGE_NAME}:latest")"
	IMAGE_VERSION="$(print_image_version_from_date \
		"$(docker cp "${container_id}":/usr/portage/metadata/timestamp.commit - | \
		tar -x -f - -O | head -n 1 | cut -d' ' -f 3)")"
	docker_commit \
		--change="LABEL org.label-schema.version='${IMAGE_VERSION}'" \
		--message="Sync tree to ${IMAGE_VERSION}" \
		"${container_id}" \
		"${IMAGE_NAME}:${IMAGE_VERSION}"
	docker rm "${container_id}"
	echo "Tagging image as latest..."
	docker tag "${IMAGE_NAME}:${IMAGE_VERSION}" "${IMAGE_NAME}:latest"
	echo "Image available as '${IMAGE_NAME}:${IMAGE_VERSION}'"
fi
