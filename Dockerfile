FROM scratch

ARG SEED_NAME
ADD "${SEED_NAME}" /
CMD ["bash"]

# Distcc port
EXPOSE 3632

ARG BUILD_DATE
ARG IMAGE_NAME
ARG IMAGE_VERSION
ARG VCS_REF

# Labels
LABEL maintainer="Philipp Richter <richterphilipp.pops@gmail.com>" \
	org.label-schema.schema-version="1.0" \
	org.label-schema.build-date="${BUILD_DATE}" \
	org.label-schema.name="${IMAGE_NAME}" \
	org.label-schema.description="Docker image builder Gentoo AMD64 glibc hardened" \
	org.label-schema.vcs-url="https://gitlab.com/rezom/docker/builder-gentoo-amd64-glibc-hardened" \
	org.label-schema.vcs-ref="${VCS_REF}" \
	org.label-schema.vendor="Rezom" \
	org.label-schema.version="${IMAGE_VERSION}" \
	org.label-schema.docker.cmd="docker run --rm -p 3632:3632 --cap-add=SYS_PTRACE --tmpfs /var/tmp:rw,nosuid,noatime,nodev,exec,size=8G,mode=1777 -ti ${IMAGE_NAME}:${IMAGE_VERSION}"
