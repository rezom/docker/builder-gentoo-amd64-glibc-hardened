# Docker builder based on Gentoo

With the use flags and overlays, this builder offers absolute flexibility when it comes to which version and how it is compiled.
Precise accounting for every package merged into an image and CVE scanning.

## Tech Specs

* Gentoo
* AMD64
* glibc as libc
* hardened
* GLSA check during build
* optimized for size
* ccache available
* distcc available
* go available

## Config files

### [`Dockerfile.template`](Dockerfile.template)

Scaffold for creating Dockerfiles that use the builder system.
Labels are based on the label schema convention : http://label-schema.org/rc1/

#### Labels
Name|Value|Info
---|---|---
`maintainer`|`@@MAINTAINER@@`|The person taking care of the docker image. Can be templated using the placeholder.
`org.label-schema.schema-version`|`1.0`|Should never be changed.
`org.label-schema.build-date`|`${BUILD_DATE}`|Will automatically be set by `docker-build.sh`.
`org.label-schema.name`|`${IMAGE_NAME}`|Will be set automatically by `docker-build.sh`.
`org.label-schema.description`|`@@DESCRIPTION@@`|Set a description pertaining to the current docker image. Can be templated using the placeholder.
`org.label-schema.vcs-url`|`@@VCS_URL@@`|The git url of the docker image. Can be templated using the placeholder.
`org.label-schema.vcs-ref`|`${VCS_REF}`|Will be filled in automatically by `docker-build.sh`.
`org.label-schema.vendor`|`@@VENDOR@@`|The vendor if this image. Can be templated.
`org.label-schema.version`|`${IMAGE_VERSION}`|Will be set automatically by `docker-build.sh`.
`org.label-schema.docker.cmd`|`@@DOCKER_CMD@@`|Set the docker command line to run this image.

There will also be additional labels referencing the sha1 checksums of the build files :

Name|Value|Info
---|---|---
`fr.rezom.docker.config-sha1`|`aff2ff9b67a7e2562b3b17051d759852a9871083`|The sha1 checksum of `config.sh`
`fr.rezom.docker.dockerfile-sha1`|`94d59667ebd83940a3e69b48fa0c7009f6c7931c`|The sha1 checksum of the `Dockerfile`

These are used to determine if the builder context should be recreated.

#### Writing the Dockerfile
In the Dockerfile a builder context will be provided, this way all development files are available for further processing.
The real root is located at `${DOCKER_TARGET_DIR}` which is `/mnt/target` by default. The variable is passed to the Dockerfile.
Any changes should be applied to this destination using `chroot` or copied there for the second part processing.
The [`Dockerfile.intermediate.template`](Dockerfile.intermediate.template) is an optional intermediate build phase that inherits the builder context.
The copy step from the `Dockerfile.template` can be changed if an intermediate phase is not needed : 

```
FROM "${IMAGE_BUILDER_INTERMEDIATE_NAME}:${IMAGE_BUILDER_INTERMEDIATE_VERSION}" AS intermediate
```
to
```
FROM "${IMAGE_BUILDER_NAME}:${IMAGE_BUILDER_VERSION}" AS intermediate
```

Best practices :
- fetch the project repo in the intermediate builder context
- install any needed dependencies in the intermediate builder context (e.g.: php modules)
- compile or generate any needed dependencies in the intermediate builder context (e.g.: nodejs modules that might require compilation)
- treat the intermediate builder context like a dev environment with all needed stuff
- copy the changes into the `${DOCKER_TARGET_DIR}` which represents the production context with the minimal components to make the application run
- copy in the final image context the ready to use `${DOCKER_TARGET_DIR}` as `/`
- apply only minimal modifications in the final image context! Remove any non-production files

### [`config.sh`](config.sh)

This file is sourced by `docker-build.sh` and contains the build information for the wrapper.

|Name|Type|Value|Info|
|---|---|---|---|
|`IMAGE_NAME`|String|`rezom/myimage`|The name of the image to be created|
|`IMAGE_VERSION`|String|`1.3.10`|The version the image should be tagged with. The version information of the builder will also be appended|
|`DOCKER_TARGET_DIR`|String|`/mnt/target`|The target directory where the production version will be put. By default it will be `/mnt/target`|
|`DOCKER_USE`|Array|`(-gpg -perl -python aio cjk cli curl)`|The global use flags to apply.|
|`DOCKER_APPEND_MAKE_CONF`|Multiline String|`PHP_TARGETS="php7-2"`|Additional configuration to append to the `make.conf`|
|`DOCKER_PACKAGE_ACCEPT_KEYWORDS`|Multiline String|`dev-php/composer ~*`|Package specific keywords changes|
|`DOCKER_PACKAGE_USE`|Multiline String|`media-gfx/imagemagick -openmp`|Package specific use changes|
|`DOCKER_PACKAGE_UNMASK`|Multiline String|`=x11-base/xorg-server-1.11.99.2`|Package unmasking|
|`DOCKER_TARGET_PACKAGES`|Array|`(app-shells/bash dev-vcs/git)`|The packages to install. Base packages already include : `(sys-apps/baselayout sys-libs/glibc sys-apps/busybox)`|
|`DOCKER_BUILDER_PACKAGES`|Array|`(app-shells/bash dev-vcs/git)`|The packages to only install into the builder context. The builder context will have those in addition to the target packages|
|`DOCKER_IGNORE_GLSAS`|Array|`(201812-09)`|Specific GLSAs to ignore|

The helper functions from [`common.sh`](common.sh) are available in the config

### [`common.sh`](common.sh)

Common utility functions

- `get_container_id_from_ancestor IMAGENAME|IMAGENAME:IMAGEVERSION`

  Gets the hash of the latest container which has IMAGENAME as ancestor.

- `print_image_version_from_date DATE`

  Prints the compact date representation to be used as version for the builder

- `get_image_version_from_image_name IMAGENAME`

  Gets the latest version string associated with the image

- `docker_run_build ...`

  Wrapper around `docker run` which has base parameters for the build context.
  Takes any valid `docker run` parameter and needs at least the image name to run.

- `docker_build ...`

  Wrapper around `docker build` with all needed switches set.
  Adding more parameters might not be needed, unless more `--build-arg` variables need to be passed.

- `docker_commit ...`

  Wrapper around `docker commit` with some base switches.
  Takes at least the container id.

- `git_print_pretty_version [gitdir]`

  Prints a pretty version string from a git directory.
  If no dir is specified then current dir is assumed.

- `get_image_id_from_image_name IMAGENAME|IMAGENAME:IMAGETAG`

  Gets the image id of the latest image name.


### [`docker-compose.yml`](docker-compose.yml)

The docker-compose file  has 2 services for now :
- cli
- distccd

To play around in the image, list packages, dependencies, use flags, test package emerge etc...
```sh
$ docker-compose run --rm cli
```
Already configured to mount tmpfs to `/var/tmp` for speeding up compilations.

The distccd service launches distccd and allows connections from any local network ip. Further speeds up compilation by distributing the work over the network.

```sh
$ docker-compose run --rm distccd
```

## Wrappers

### [`docker-update.sh`](docker-update.sh)

Updates the builder image or creates it from the latest seed located in the directory if no image is available.

### [`docker-export-seed.sh`](docker-export-seed.sh)

Exports the latest image of the builder as new seed to be used as base for a new image.
Signs the image with the gpg key.

### [`docker-build.sh`](docker-build.sh)

Expects a `config.sh` and a `Dockerfile` in the directory it is invoked in.
The `config.sh` defines the variables to be used during the build and the `Dockerfile` builds the final image from this builder image.
Optionally there can be a `Dockerfile.intermediate` which describes build steps in the builder image.
If a `patches` directory is present, it will be mounted read-only inside the container at `/etc/portage/patches`.

## TODO

- [ ] Proper distcc testing
- [ ] distcc integration into the build process
- [ ] overlay handling via config variables
