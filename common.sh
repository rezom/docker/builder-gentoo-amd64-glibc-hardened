# param1 : ancestor image
function get_container_id_from_ancestor {
	docker ps --all --quiet --last 1 --filter "ancestor=${1:-}"
}

# param1 : date
function print_image_version_from_date {
	date +'%Y%m%d%H%M%S' --date="${1:-}"
}

# param1 : image_name
function get_image_version_from_image_name {
	docker inspect \
		--type=image \
		--format="{{index .RepoTags 0}}" \
		"${1:-}" | cut -d: -f 2
}

# params : image_name and any other params to pass to docker run
function docker_run_build {
	docker run \
		--cap-add=SYS_PTRACE \
		--tmpfs /var/tmp:rw,nosuid,noatime,nodev,exec,size=8G,mode=1777 \
		--mount type=volume,source=builder-gentoo-amd64-glibc-hardened_ccache,destination=/var/cache/ccache \
		--interactive \
		"${@}"
}

# params : any params to pass to docker build
function docker_build {
	DOCKERFILE="${DOCKERFILE:-Dockerfile}"
	# --squash needs dockerd running with --experimental
	docker build \
		--label="fr.rezom.docker.config-sha1=$(sha1sum ./config.sh | cut -d' ' -f 1)" \
		--label="fr.rezom.docker.dockerfile-sha1=$(sha1sum "${DOCKERFILE}" | cut -d' ' -f 1)" \
		--build-arg=IMAGE_BUILDER_NAME="${IMAGE_BUILDER_NAME:-@@IMAGE_BUILDER_NAME@@}" \
		--build-arg=IMAGE_BUILDER_VERSION="${IMAGE_BUILDER_VERSION:-@@IMAGE_BUILDER_NAME@@}" \
		--build-arg=IMAGE_BUILDER_INTERMEDIATE_NAME="${IMAGE_BUILDER_INTERMEDIATE_NAME:-@@IMAGE_BUILDER_INTERMEDIATE_NAME@@}" \
		--build-arg=IMAGE_BUILDER_INTERMEDIATE_VERSION="${IMAGE_BUILDER_INTERMEDIATE_VERSION:-@@IMAGE_BUILDER_INTERMEDIATE_NAME@@}" \
		--build-arg=BUILD_DATE="$(date -u +'%Y-%m-%dT%H:%M:%SZ')" \
		--build-arg=IMAGE_NAME="${IMAGE_NAME:-@@IMAGE_NAME@@}" \
		--build-arg=IMAGE_VERSION="${IMAGE_VERSION:-@@IMAGE_VERSION@@}" \
		--build-arg=VCS_REF="$(git rev-parse --short HEAD || echo "@@VCS_REF@@")" \
		--tag "${IMAGE_NAME:-}:${IMAGE_VERSION:-}" \
		--file "${DOCKERFILE}" \
		"${@}" \
		.
}

# params : any to be passed to docker commit
function docker_commit {
	docker commit \
		--author="${AUTHOR:-@@AUTHOR@@}" \
		--change="LABEL fr.rezom.docker.config-sha1='$(sha1sum ./config.sh | cut -d' ' -f 1)'" \
		"${@}"
}

# param1 : git directory or empty for current dir
function git_print_pretty_version {
	git --git-dir "${1:-.}"/.git describe --long --tags | \
		sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

# param1 : image name to get the image id from
function get_image_id_from_image_name {
	docker images --quiet "${1:-}" | head -n 1
}
